package com.example.geoserverdemo.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.postgis.PGgeometry;
import org.postgis.Point;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes({CPoint.class})
public class PointHandler extends BaseTypeHandler<CPoint> {

    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, CPoint cPoint, JdbcType jdbcType) throws SQLException {

    }

    @Override
    public CPoint getNullableResult(ResultSet resultSet, String s) throws SQLException {
        PGgeometry pGgeometry = (PGgeometry) resultSet.getObject(s);
        Point point = pGgeometry.getGeometry().getPoint(0);

        CPoint cPoint = new CPoint();
        cPoint.setCoordinates(new double[]{point.x, point.y});
        return cPoint;
    }

    @Override
    public CPoint getNullableResult(ResultSet resultSet, int i) throws SQLException {
        return null;
    }

    @Override
    public CPoint getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
        return null;
    }
}
