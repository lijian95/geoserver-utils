package com.example.geoserverdemo.handler;

import lombok.Data;

import java.util.List;

@Data
public class CLine {
    private List<double[]> coordinates;
}
