package com.example.geoserverdemo.handler;

import lombok.Data;

import java.util.List;

@Data
public class CMLine {
    private List<List<double[]>> coordinates;
}
