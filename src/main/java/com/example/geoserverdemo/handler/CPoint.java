package com.example.geoserverdemo.handler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
public class CPoint {
    double[] coordinates;
}
