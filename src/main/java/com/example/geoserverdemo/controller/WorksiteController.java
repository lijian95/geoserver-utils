package com.example.geoserverdemo.controller;

import com.example.geoserverdemo.dao.AxisMapper;
import com.example.geoserverdemo.dao.ProtectionMapper;
import com.example.geoserverdemo.dao.StationMapper;
import com.example.geoserverdemo.handler.CPoint;
import com.example.geoserverdemo.po.*;
import com.example.geoserverdemo.util.GeometryFactoryHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.locationtech.jts.geom.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController("/")
@Api(tags = "工点管理接口")
public class WorksiteController {

    @Autowired
    StationMapper stationMapper;

    @Autowired
    AxisMapper axisMapper;

    @Autowired
    ProtectionMapper protectionMapper;

    @ApiOperation(value = "获取站点", notes = "根据站点名称获取站点,支持模糊查找")
    @ApiImplicitParam(paramType = "query", name = "name", value = "站点名称", required = true)
    @GetMapping("/selectStation")
    public List<Station> selectStation(@RequestParam String name){
        StationExample stationExample = new StationExample();
        stationExample.createCriteria().andSiteNameLike("%" + name + "%");
        return stationMapper.selectByExample(stationExample);
    }

    @ApiOperation(value = "获取轴线", notes = "根据线路名称获取线路, 支持模糊查找")
    @ApiImplicitParam(paramType = "query", name = "name", value = "线路名称", required = true)
    @GetMapping("/selectAxis")
    public Map<String, Object> selectAxis(@RequestParam String name) throws IllegalAccessException {
        Map<String, Object> result = new HashMap<>();

        AxisExample axisExample = new AxisExample();
        axisExample.createCriteria().andLineNameLike("%" + name + "%");
        List<Axis> axisList = axisMapper.selectByExample(axisExample);

        // 插入中心点
        for(Axis axis : axisList){
            LineString lineString = GeometryFactoryHelper.createLineString(axis.getGeometry().getCoordinates());
            Point point = lineString.getCentroid();
            for(Field field: Axis.class.getDeclaredFields()){
                field.setAccessible(true);
                result.put(field.getName(), field.get(axis));
            }
            result.put("center", new double[]{point.getX(), point.getY()});
        }

        return result;
    }

    @ApiOperation(value = "获取保护区", notes = "根据保护区名称获取保护区, 支持模糊查找")
    @ApiImplicitParam(paramType = "query", name = "name", value = "保护区名称", required = true)
    @GetMapping("/selectProtection")
    public Map<String, Object> selectProtection(@RequestParam String name) throws IllegalAccessException {
        Map<String, Object> result = new HashMap<>();

        ProtectionExample protectionExample = new ProtectionExample();
        protectionExample.createCriteria().andLineNameLike("%" + name + "%");
        List<Protection> protectionList =  protectionMapper.selectByExample(protectionExample);

        // 插入中心点
        for(Protection protection : protectionList){
            MultiLineString multiLineString = GeometryFactoryHelper.createMultiLineString(protection.getGeometry().getCoordinates());

            Point point = multiLineString.getCentroid();
            for(Field field: Protection.class.getDeclaredFields()){
                field.setAccessible(true);
                result.put(field.getName(), field.get(protection));
            }
            result.put("center", new double[]{point.getX(), point.getY()});
        }

        return result;
    }
}
