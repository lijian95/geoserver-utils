package com.example.geoserverdemo;

import com.example.geoserverdemo.util.PublishHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value = "com.example.geoserverdemo.dao")
public class GeoserverDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeoserverDemoApplication.class, args);

    }
}
