package com.example.geoserverdemo.util;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.locationtech.jts.geom.*;

import java.util.ArrayList;
import java.util.List;

public class GeometryFactoryHelper {
    public static GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();

    public static Point createPoint(double[] coord){
        Coordinate coordinate = new Coordinate(coord[0], coord[1]);
        Point point = geometryFactory.createPoint(coordinate);
        return point;
    }

    public static LineString createLineString(List<double[]> coords){
        Coordinate[] lineCoords = new Coordinate[coords.size()];
        for(int i = 0; i < coords.size(); i++){
            lineCoords[i] = new Coordinate(coords.get(i)[0], coords.get(i)[1]);
        }

        LineString lineString = geometryFactory.createLineString(lineCoords);
        return lineString;
    }

    public static MultiLineString createMultiLineString(List<List<double[]>> coords){
       List<LineString> result = new ArrayList<>();
       for(List<double[]> coord: coords) {
           LineString lineString = createLineString(coord);
           result.add(lineString);
       }
       MultiLineString multiLineString = geometryFactory.createMultiLineString(result.toArray(new LineString[result.size()]));
       return multiLineString;
    }

    public static Polygon createPolygon(List<double[]> coords){
        Coordinate[] polyCoords = new Coordinate[coords.size()];
        for(int i = 0; i < coords.size(); i++){
            polyCoords[i] = new Coordinate(coords.get(i)[0], coords.get(i)[1]);
        }

        LinearRing ring = geometryFactory.createLinearRing(polyCoords);
        LinearRing holes[] = null;
        Polygon polygon = geometryFactory.createPolygon(ring, holes);
        return polygon;
    }
}
