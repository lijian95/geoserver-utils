package com.example.geoserverdemo.dao;

import com.example.geoserverdemo.po.Axis;
import com.example.geoserverdemo.po.AxisExample;
import java.util.List;

public interface AxisMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Axis record);

    int insertSelective(Axis record);

    List<Axis> selectByExample(AxisExample example);

    Axis selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Axis record);

    int updateByPrimaryKey(Axis record);
}