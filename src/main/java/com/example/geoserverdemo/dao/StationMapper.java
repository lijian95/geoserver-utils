package com.example.geoserverdemo.dao;

import com.example.geoserverdemo.po.Station;
import com.example.geoserverdemo.po.StationExample;
import java.util.List;

public interface StationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Station record);

    int insertSelective(Station record);

    List<Station> selectByExample(StationExample example);

    Station selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Station record);

    int updateByPrimaryKey(Station record);
}