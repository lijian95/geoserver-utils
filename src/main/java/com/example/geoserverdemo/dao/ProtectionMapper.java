package com.example.geoserverdemo.dao;

import com.example.geoserverdemo.po.Protection;
import com.example.geoserverdemo.po.ProtectionExample;
import java.util.List;

public interface ProtectionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Protection record);

    int insertSelective(Protection record);

    List<Protection> selectByExample(ProtectionExample example);

    Protection selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Protection record);

    int updateByPrimaryKey(Protection record);
}