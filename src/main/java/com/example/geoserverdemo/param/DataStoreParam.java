package com.example.geoserverdemo.param;

import lombok.Data;

import java.util.Map;

@Data
public class DataStoreParam {
    private Map<String, Object> dataStore;
}
