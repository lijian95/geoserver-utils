package com.example.geoserverdemo.param;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class WorkspaceParam {
    private Map<String, String> workspace;
}
