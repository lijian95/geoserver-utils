package com.example.geoserverdemo.param;

import com.example.geoserverdemo.dto.FeatureType;
import lombok.Data;

@Data
public class FeatureTypeParam {
    private FeatureType featureType;
}
