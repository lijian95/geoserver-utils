package com.example.geoserverdemo.dto;

import lombok.Data;

@Data
public class FeatureType {
        private String name;
        private String nativeName;
        private String nativeCRS;
        private String srs;
        private String projectionPolicy;
        private Boolean enabled;
        private Boolean serviceConfiguration;
        private Boolean simpleConversionEnabled;
        private String internationalTitle;
        private String internationalAbstract;
        private Integer maxFeatures;
        private Integer numDecimals;
        private Boolean padWithZeros;
        private Boolean forcedDecimal;
        private Boolean overridingServiceSRS;
        private Boolean skipNumberMatched;
        private Boolean circularArcPresent;
}
