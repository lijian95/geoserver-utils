package com.example.geoserverdemo;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;
import reactor.core.publisher.Mono;

import java.util.HashMap;

import static com.fasterxml.jackson.databind.type.LogicalType.Map;

@SpringBootTest
class GeoserverDemoApplicationTests {

    @Test
    void get(){
        WebClient webClient = WebClient.create("http://172.16.1.74:8080/geoserver/TY-304/ows");
        Mono<FC> fc = webClient.get().uri("?service=WFS&version=1.0.0&request=GetFeature&typeName=TY-304:TY304_Axis&maxFeatures=1&outputFormat=application/json&cql_filter=LineCode='S3'")
                .retrieve()
                .bodyToMono(FC.class);
        System.out.println(fc.block());
    }


    @Test
    void post(){
        WebClient webClient = WebClient.create("https://restapi.amap.com/v5/place/around");

        Param1 param1 = new Param1("厕所", "117.020025,36.645404", "92dd29bb8a55c8c303e618bb9e61b868");
        Mono<AMAP> mp = webClient.post()
                .body(Mono.just(param1), Param1.class)
                .retrieve()
                .bodyToMono(AMAP.class);

        System.out.println(mp.block());
    }
}
