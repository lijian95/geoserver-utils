package com.example.geoserverdemo;

import org.gdal.gdal.*;
import org.gdal.gdalconst.gdalconstConstants;
import org.junit.jupiter.api.Test;
import org.opengis.referencing.FactoryException;

public class Gdal {

    @Test
    public void tiffInfo() throws FactoryException {
        gdal.AllRegister();
        gdal.SetConfigOption("GDAL_DATA", "src/main/resources/static/gdal-data");
        //读取影像数据
        Dataset dataset = gdal.Open("src/main/resources/static/Landsat_TM_year_1985.tif", gdalconstConstants.GA_ReadOnly);
        if(dataset == null){
            System.err.println("GDALOpen failed - "+gdal.GetLastErrorNo());
            System.err.println(gdal.GetLastErrorMsg());
            System.exit(1);
        }

        Driver driver = dataset.GetDriver();
        dataset.SetProjection("PROJCS[\"WGS 84 / Pseudo-Mercator\", \n" +
                "  GEOGCS[\"WGS 84\", \n" +
                "    DATUM[\"World Geodetic System 1984\", \n" +
                "      SPHEROID[\"WGS 84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7030\"]], \n" +
                "      AUTHORITY[\"EPSG\",\"6326\"]], \n" +
                "    PRIMEM[\"Greenwich\", 0.0, AUTHORITY[\"EPSG\",\"8901\"]], \n" +
                "    UNIT[\"degree\", 0.017453292519943295], \n" +
                "    AXIS[\"Geodetic latitude\", NORTH], \n" +
                "    AXIS[\"Geodetic longitude\", EAST], \n" +
                "    AUTHORITY[\"EPSG\",\"4326\"]], \n" +
                "  PROJECTION[\"Popular Visualisation Pseudo Mercator\", AUTHORITY[\"EPSG\",\"1024\"]], \n" +
                "  PARAMETER[\"semi_minor\", 6378137.0], \n" +
                "  PARAMETER[\"latitude_of_origin\", 0.0], \n" +
                "  PARAMETER[\"central_meridian\", 0.0], \n" +
                "  PARAMETER[\"scale_factor\", 1.0], \n" +
                "  PARAMETER[\"false_easting\", 0.0], \n" +
                "  PARAMETER[\"false_northing\", 0.0], \n" +
                "  UNIT[\"m\", 1.0], \n" +
                "  AXIS[\"Easting\", EAST], \n" +
                "  AXIS[\"Northing\", NORTH], \n" +
                "  AUTHORITY[\"EPSG\",\"3857\"]]");

        driver.CreateCopy("src/main/resources/static/copy_tiff5.tif", dataset);

        dataset.delete();
        gdal.GDALDestroyDriverManager();
    }
}
