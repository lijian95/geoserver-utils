package com.example.geoserverdemo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Param1 {
    private String keywords;
    private String location;
    private String key;
}
