package com.example.geoserverdemo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class FC implements Serializable {
    private String type;
    private List<Object> features;
    private Integer totalFeatures;
    private Integer numberMatched;
    private Integer numberReturned;
    private String timeStamp;
    private Object crs;
}
