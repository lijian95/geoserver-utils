package com.example.geoserverdemo;

import lombok.Data;

import java.util.List;

@Data
public class AMAP {
    private Integer status;
    private String info;
    private String infocode;
    private List<Object> pois;
    private Integer count;
}
