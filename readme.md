## Mybatis Generator 处理地理坐标

### 运行 

1. 配置数据库链接
2. 修改resources/mybatis-generator-config.xml数据表映射
3. 运行 mbg.Generator
4. 运行程序，调用接口

    ![](./src/main/resources/static/r.png)


### 其他
**添加地理坐标类**
```java
import lombok.Data;
@Data
public class CPoint {
    double[] coordinates;
}

@Data
public class CLine {
    private List<double[]> coordinates;
}

```

**添加地理坐标数据类型处理**
```java

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.postgis.PGgeometry;
import org.postgis.Point;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes({CPoint.class})
public class PointHandler extends BaseTypeHandler<CPoint> {

    @Override
    // 重写CPoint getter 方法, 其他方法可以为默认值
    public CPoint getNullableResult(ResultSet resultSet, String s) throws SQLException {
        PGgeometry pGgeometry = (PGgeometry) resultSet.getObject(s);
        Point point = pGgeometry.getGeometry().getPoint(0);

        CPoint cPoint = new CPoint();
        cPoint.setCoordinates(new double[]{point.x, point.y});
        return cPoint;
    }
}


@MappedTypes(CLine.class)
public class LineHandler extends BaseTypeHandler<CLine> {
    
    @Override
    public CLine getNullableResult(ResultSet resultSet, String s) throws SQLException {
        List<double[]> coordinates = new ArrayList<>();

        PGgeometry pGgeometry = (PGgeometry) resultSet.getObject(s);
        // 正常的数据类型应该为LineString的
        // 因为数据库里面是MultiLineString, 所以我取了第一条多段线
        MultiLineString multiLineString = (MultiLineString) pGgeometry.getGeometry();
        if(multiLineString.getLines().length == 0) return null;

        LineString firstLine = multiLineString.getLine(0);
        for (Point point : firstLine.getPoints()){
            double[] coord = new double[]{point.x, point.y};
            coordinates.add(coord);
        }
        CLine cLine = new CLine();
        cLine.setCoordinates(coordinates);
        return cLine;
    }
}

```
参考链接：
1. [mybatis generator xml配置](https://mybatis.org/generator/configreference/xmlconfig.html)
2. [MyBatis Generator 超详细配置](https://juejin.cn/post/6844903982582743048)

注：省略的配置数据：
1) src/resources/mybatis-config.xml
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="org.postgresql.Driver"/>
                <property name="url" value="jdbc:postgresql://0.0.0.0:5432/database"/>
                <property name="username" value=""/>
                <property name="password" value=""/>
            </dataSource>
        </environment>
    </environments>
</configuration>

```
2) src/resource/application.properties
```
server.port=9000

# 数据库配置
spring.datasource.driverClassName=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://0.0.0.0:5432/database
spring.datasource.username=
spring.datasource.password=
spring.jackson.serialization.fail-on-empty-beans=false


# Mybatis 配置
mybatis.mapperLocations=classpath:mapper/*.xml
```